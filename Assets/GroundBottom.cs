﻿using UnityEngine;
using System.Collections;

public class GroundBottom : MonoBehaviour {

	private BoxCollider2D boxCollider;
	private Ground ground;

	void Awake () {
		boxCollider = GetComponent<BoxCollider2D> ();
		ground = transform.parent.GetComponent<Ground> ();
	}

	void OnCollisionEnter2D(Collision2D col) {
		if (col.transform.tag == "Enemy") {
			Enemy enemyControl = col.gameObject.GetComponent<Enemy>();
			enemyControl.isTrapped = true;
		}
		ground.objectInTrap = col.gameObject;
	}

	void OnTriggerExit2D (Collider2D other) {
		boxCollider.isTrigger = false;
	}

	public void SetTrappedObject(GameObject objectInTrap) {
		ground.objectInTrap = objectInTrap;
	}
}
