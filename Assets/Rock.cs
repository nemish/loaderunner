﻿using UnityEngine;
using System.Collections;

public class Rock : MonoBehaviour, Destroyable {

	public void DestroyByBrickWall() {
		Destroy (gameObject);
	}
}
