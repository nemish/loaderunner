﻿using UnityEngine;
using System.Collections;

public class RopeScript : MonoBehaviour {
	private GameObject player;
	private PlayerControl playerControl;

	void Awake () {
		player = GameObject.Find("hero");
		playerControl = player.GetComponent<PlayerControl>();
	}

	void OnTriggerEnter2D (Collider2D col) 
	{
		if (!col.sharedMaterial) {
			if(col.tag == "Player")
			{
				Debug.Log("OnRope");
				if (playerControl.canClimbRope){
					playerControl.SetOnRope(true);
				}
			}		
		}
	}
	void OnTriggerExit2D (Collider2D col) 
	{
		if (!col.sharedMaterial) {
			if(col.tag == "Player")
			{
				playerControl.SetOnRope(false);
			}
		}
		
	}
}
