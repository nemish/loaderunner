using UnityEngine;
using System.Collections;

public class Win : MonoBehaviour {

    public Texture bgTexture;
    private int buttonWidth = 200;
    private int buttonHeight = 50;

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), bgTexture);
 //       GUI.Label(new Rect(10, 10, 100, 50), "Final Score:" + Player.Score * Player.Lives);
        if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2,
                       Screen.height / 2 - buttonHeight / 2, buttonWidth, buttonHeight), "You Win!\nRestart"))
        {
//            Player.Score = 0;
//            Player.Lives = 3;
//           Player.Hits = 0;
            Application.LoadLevel(0);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
            Application.Quit();
    }
}
