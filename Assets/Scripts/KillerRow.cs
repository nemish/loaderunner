﻿using UnityEngine;
using System.Collections;

public class KillerRow : MonoBehaviour {

	public float letalSpeed = 1.0f;
	public GameObject explosion;
	private CircleCollider2D rockCollider;
	private float speed;
	private Enemy enemy;

	// Use this for initialization
	void Start () {
		rockCollider = GetComponent<CircleCollider2D>();
	}
	
	// Update is called once per frame
	void Update () {

	}

	void OnCollisionEnter2D (Collision2D col) {
//		float speed = Mathf.Sqrt(Mathf.Pow(rockCollider.rigidbody2D.velocity.x, 2) + Mathf.Pow(rockCollider.rigidbody2D.velocity.y, 2));
		float speed = rockCollider.rigidbody2D.velocity.magnitude;
//		Debug.Log(speed);

		if (col.gameObject.tag == "Player") {
			Vector2 hit = col.contacts[0].normal;
			
			float angle = Vector2.Angle(hit,Vector2.right);
			
//			Debug.Log(angle);

			if (angle == 90 && speed >= letalSpeed)
			{
				PlayerHealth ph = col.gameObject.GetComponent<PlayerHealth>();
				ph.Death();
			} 
		}

		if(col.gameObject.tag == "Enemy" && speed >= letalSpeed)
		{
			Debug.Log(speed);
			enemy = col.gameObject.GetComponent<Enemy>();
			enemy.Death();
			Destroy(gameObject);
			OnExplode();
		}

/*		if(col.gameObject.tag == "Destroyable" && speed >= letalSpeed * 2)
		{
			Debug.Log(speed);
			Ground ground = col.gameObject.GetComponent<Ground>();
			ground.HideBlock();
			Destroy(gameObject);
			OnExplode();
		} */

	}

	void OnExplode()
	{
		// Create a quaternion with a random rotation in the z-axis.
		Quaternion randomRotation = Quaternion.Euler(0f, 0f, Random.Range(0f, 360f));
		
		// Instantiate the explosion where the rocket is with the random rotation.
		Instantiate(explosion, transform.position, randomRotation);
	}
}
