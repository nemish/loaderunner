﻿using UnityEngine;
using System.Collections;

public class Remover : MonoBehaviour
{
	public Transform[] enemySpawnPoints;

	void OnTriggerExit2D(Collider2D col)
	{
		// If the player hits the trigger...
		if(col.gameObject.tag == "Player")
		{
			// .. stop the camera tracking the player
			GameObject camera = GameObject.FindGameObjectWithTag("MainCamera");
			camera.GetComponent<CameraFollow>().enabled = false;

			// .. stop the Health Bar following the player
/*			if(GameObject.FindGameObjectWithTag("HealthBar").activeSelf)
			{
				GameObject.FindGameObjectWithTag("HealthBar").SetActive(false);
			}
*/


			if (camera.GetComponent<Lifes>().lifes-- <= 0) {
				StartCoroutine("ReloadGame");
				return;
			}

			PlayerControl player = col.gameObject.GetComponent<PlayerControl>();
			PlayerHealth playerHealth = col.gameObject.GetComponent<PlayerHealth>();

			// Find all of the colliders on the gameobject and set them all to be triggers.
			Collider2D[] cols = playerHealth.GetComponents<Collider2D>();
			foreach(Collider2D c in cols)
			{
				c.isTrigger = false;
			}
			
			// Move all sprite parts of the player to the front
			SpriteRenderer[] spr = playerHealth.GetComponentsInChildren<SpriteRenderer>();
			foreach(SpriteRenderer s in spr)
			{
				s.sortingLayerName = "player";
			}

			playerHealth.Restart();

			col.gameObject.transform.position = player.startingPosition.transform.position;
			player.isDead = false;
			// ... destroy the player.
//			Destroy (col.gameObject);
			
//			Instantiate(col.gameObject, player.startingPosition.transform.position, Quaternion.identity);

			camera.GetComponent<CameraFollow>().enabled = true;
			camera.GetComponent<CameraFollow>().Restart();

		} else if (col.gameObject.tag == "Enemy") {
			Destroy (col.gameObject);
			Enemy enemyControl = col.gameObject.GetComponent<Enemy>();
			enemyControl.Reset();
			int i = Random.Range (0, enemySpawnPoints.Length);
			Instantiate(col.gameObject, enemySpawnPoints[i].position, Quaternion.identity);

		} else {
			if (col.gameObject.tag != "Player") {
				// Destroy the enemy.
				Destroy (col.gameObject);	
			}
		}
	}

	IEnumerator ReloadGame()
	{			
		// ... pause briefly
		yield return new WaitForSeconds(2);
		// ... and then reload the level.
		triggerGold.gold = 0;
		Application.LoadLevel(Application.loadedLevel);
	}
}
