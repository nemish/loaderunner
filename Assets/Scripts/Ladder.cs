﻿using UnityEngine;
using System.Collections;

public class Ladder : MonoBehaviour {

	private GameObject player;
	private PlayerControl playerControl;

	void Awake () {
		player = GameObject.FindGameObjectWithTag("Player");
		playerControl = player.GetComponent<PlayerControl>();
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
			
	}

	void OnTriggerEnter2D (Collider2D col) 
	{
		if(col.tag == "Player")
		{
			// ... find the Enemy script and call the Hurt function.
			playerControl.SetCanClimb(true);
			playerControl.SetLadder(gameObject);
		}

	}

	void OnTriggerExit2D (Collider2D col) {
		if(col.tag == "Player")
		{
			// ... find the Enemy script and call the Hurt function.
			playerControl.SetCanClimb(false);
			
		}
	}
	
}