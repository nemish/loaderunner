﻿using UnityEngine;
using System.Collections;

public class Ground : MonoBehaviour {
	public GameObject objectInTrap = null;
	public bool isSomethingInTrap = false;
	private Collider2D boxCollider;

	void Awake()
	{
		boxCollider = GetComponent<Collider2D>();
	}

	public void HideBlock () {
		renderer.enabled = false;
		boxCollider.isTrigger = true;
		StartCoroutine(WaitAndRestore(6.0f));
	}

	IEnumerator WaitAndRestore(float waitTime) {
		yield return new WaitForSeconds(waitTime);
		if (objectInTrap) {
			string objectTag = objectInTrap.transform.tag;
			if (objectTag == "Enemy") {
				Enemy objectControl = objectInTrap.gameObject.GetComponent<Enemy>();
				objectControl.DestroyByBrickWall();
			} else if (objectTag == "Player") {
				PlayerHealth objectControl = objectInTrap.gameObject.GetComponent<PlayerHealth>();
				objectControl.DestroyByBrickWall();
			} else if (objectTag == "Rock") {
				Rock objectControl = objectInTrap.gameObject.GetComponent<Rock>();
				objectControl.DestroyByBrickWall();
			}
			objectInTrap = null;
		}
		renderer.enabled = true;
		boxCollider.isTrigger = false;
	}

}
