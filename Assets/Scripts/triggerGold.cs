﻿using UnityEngine;
using System.Collections;

public class triggerGold : MonoBehaviour {

	public static int gold;

	void OnTriggerEnter2D (Collider2D col) 
	{
		if(col.tag == "Player")
		{
			Destroy(gameObject);
			gold++;
		}

		if (CameraGoldPoints.goldAtLevel == gold)
			WinTrigger.instance.StartCoroutine(WinTrigger.instance.BuildLadder());
	}
}
