﻿using UnityEngine;
using System.Collections;

public interface Destroyable 
{
	void DestroyByBrickWall();
}