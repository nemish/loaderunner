﻿using UnityEngine;
using System.Collections;

public class Lifes : MonoBehaviour {
	public int lifes = 3;
	public Texture heartTexture;

	private const float L_SIZE = 64.0f;

	// Use this for initialization
	void Start () {
	
	}

	void OnGUI() {
		for (int i = 0; i < lifes; i++) {
			GUI.DrawTexture(new Rect(10 + L_SIZE*i, 0, L_SIZE, L_SIZE), heartTexture);
		}
	}
	
	// Update is called once per frame
	void Update () {

	}
}
