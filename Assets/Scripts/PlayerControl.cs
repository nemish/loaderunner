﻿using UnityEngine;
using System.Collections;

public class PlayerControl : MonoBehaviour
{
	[HideInInspector]
	public bool facingRight = true;			// For determining which way the player is currently facing.
	[HideInInspector]
	public bool jump = false;				// Condition for whether the player should jump.
	public bool isDead = false;
	
	public float moveForce = 50f;			// Amount of force added to move the player left and right.
	public float maxSpeed = 4f;				// The fastest the player can travel in the x axis.
	public float climbSpeed = 4f;
	public AudioClip[] jumpClips;			// Array of clips for when the player jumps.
	public float jumpForce = 300f;			// Amount of force added when the player jumps.

	public AudioClip[] taunts;				// Array of clips for when the player taunts.
	public float tauntProbability = 50f;	// Chance of a taunt happening.
	public float tauntDelay = 1f;			// Delay for when the taunt should happen.

	public GameObject startingPosition;
	
	
	private int tauntIndex;					// The index of the taunts array indicating the most recent taunt.
	private Transform groundCheck;			// A position marking where to check if the player is grounded.
	private Transform groundBottomCheck;


	private RaycastHit2D blockUnderPlayer;			// Whether or not the player is blockUnderPlayer.
//	private bool onLadder = false;
	private Animator anim;					// Reference to the player's animator component.
	
	private bool canClimb = false;
	private float initialMass;
	private bool isClimb = false;
	public bool canClimbRope = true; //хак, чтобы можно было спуститься с веревки
	private bool isOnRope = false;
	private Transform frontCheck;
	private Gun gun;
	private GameObject ladder;
	private Transform currentTrappBrick; 
	
	void Awake()
	{
		// Setting up references.
		groundCheck = transform.Find("groundCheck");
		groundBottomCheck = transform.Find("groundBottomCheck");
		anim = GetComponent<Animator>();
		initialMass = rigidbody2D.mass;

		frontCheck = transform.Find("frontCheck").transform;
		gun = GameObject.Find ("Gun").GetComponent<Gun>();
		gameObject.transform.position = startingPosition.transform.position;

	}
	
	
	void Update()
	{
		// The player is grounded if a linecast to the groundcheck position hits anything on the ground layer.
		blockUnderPlayer = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));  
//		onLadder = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));  

		// If the jump button is pressed and the player is grounded then the player should jump.
		if(Input.GetButtonDown("Jump") && blockUnderPlayer){
			jump = true;
		}

	}

	void FixedUpdate ()
	{

		// Cache the horizontal input.
		if (!isDead)
		{
			MovementManagement();
		} else {
			rigidbody2D.drag = 0;
			rigidbody2D.gravityScale = 1;
		}

	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject.tag == "Destroyable" && other.isTrigger && currentTrappBrick) {
			GroundBottom groundBottom = currentTrappBrick.GetComponent<GroundBottom> ();
			groundBottom.SetTrappedObject (null);
			currentTrappBrick = null;
		}
	}


	void MovementManagement () {

		if (currentTrappBrick && rigidbody2D.velocity.y == 0) {
			GroundBottom groundBottom = currentTrappBrick.GetComponent<GroundBottom> ();
			groundBottom.SetTrappedObject (gameObject);
		}


		float h = Input.GetAxis("Horizontal");
		
		
		if (blockUnderPlayer && blockUnderPlayer.transform.gameObject.tag == "Ladder") {
			isClimb = true;
		} else {
			isClimb = false;
		}
		
		if (blockUnderPlayer && blockUnderPlayer.transform.gameObject.tag == "Destroyable" && blockUnderPlayer.collider.isTrigger) {
			RaycastHit2D[] brickParts = Physics2D.LinecastAll(groundCheck.position, groundBottomCheck.position, 1 << LayerMask.NameToLayer("Ground"));
			foreach (RaycastHit2D part in brickParts) {
				if (part && part.collider.tag == "BrickBottom") {
					currentTrappBrick = part.transform;
					part.collider.isTrigger = true;
				}
			}
			
		}
		
		// The Speed animator parameter is set to the absolute value of the horizontal input.
		anim.SetFloat("Speed", Mathf.Abs(h));
		
		// If the player is changing direction (h has a different sign to velocity.x) or hasn't reached maxSpeed yet...
		if(h * rigidbody2D.velocity.x < maxSpeed)
			// ... add a force to the player.
			rigidbody2D.AddForce(Vector2.right * h * moveForce);
		
		// If the player's horizontal velocity is greater than the maxSpeed...
		if(Mathf.Abs(rigidbody2D.velocity.x) > maxSpeed)
			// ... set the player's velocity to the maxSpeed in the x axis.
			rigidbody2D.velocity = new Vector2(Mathf.Sign(rigidbody2D.velocity.x) * maxSpeed, rigidbody2D.velocity.y);
		
		// If the input is moving the player right and the player is facing left...
		if(h > 0 && !facingRight)
			// ... flip the player.
			Flip();
		// Otherwise if the input is moving the player left and the player is facing right...
		else if(h < 0 && facingRight)
			// ... flip the player.
			Flip();
		
		// If the player should jump...
		if(jump)
		{
			// Set the Jump animator trigger parameter.
			anim.SetTrigger("Jump");
			
			// Play a random jump audio clip.
			int i = Random.Range(0, jumpClips.Length);
			AudioSource.PlayClipAtPoint(jumpClips[i], transform.position);
			
			// Add a vertical force to the player.
			rigidbody2D.AddForce(new Vector2(0f, jumpForce));
			
			// Make sure the player can't jump again until the jump conditions from Update are satisfied.
			jump = false;
		}
		
		if (canClimb) {
			if (Input.GetKey("w")) {
				isClimb = true;
				rigidbody2D.velocity = new Vector2(-10 * (transform.position.x - ladder.transform.position.x), climbSpeed);
				//				gameObject.transform.position = new Vector2(ladder.transform.position.x, climbSpeed * Time.deltaTime);
				anim.SetTrigger ("Jump");
			} else if (Input.GetKey("s")) {
				isClimb = true;
				rigidbody2D.velocity = new Vector2(-10 * (transform.position.x - ladder.transform.position.x), -1 * climbSpeed);
				//				gameObject.transform.position = new Vector2(ladder.transform.position.x, -1 * climbSpeed * Time.deltaTime);
				anim.SetTrigger ("Jump");
			} else if (isClimb) {
				rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 0);
			}
			
		}
		if (isOnRope){
			if (Input.GetKey("s")) {
				canClimbRope = false;
				isOnRope = false;
			}
		}
		
		if (isClimb || isOnRope) {
			rigidbody2D.gravityScale = 0;
			rigidbody2D.drag = 10;
		} else {
			rigidbody2D.drag = 0;
			rigidbody2D.gravityScale = 1;
		}
		CheckCanDropBomb ();
	}

	void CheckCanDropBomb () {
		int GroundLayerIndex = 12;
		Collider2D[] frontHits = Physics2D.OverlapPointAll(frontCheck.position, 1 << GroundLayerIndex);
		
		// Check each of the colliders.
		if (frontHits.Length != 0) {
			foreach(Collider2D c in frontHits)
			{
				// If any of the colliders is an Destroyable...
				if(c.tag == "Destroyable")
				{
					gun.canDropBomb = false;
					break;
				} else {
					gun.canDropBomb = true;
					break;
				}
			}
		} else {
			gun.canDropBomb = true;
		}
	}
	
		
	void Flip ()
	{
		// Switch the way the player is labelled as facing.
		facingRight = !facingRight;
		
		// Multiply the player's x local scale by -1.
		Vector3 theScale = transform.localScale;
		theScale.x *= -1;
		transform.localScale = theScale;
	}
	
	
	public IEnumerator Taunt()
	{
		// Check the random chance of taunting.
		float tauntChance = Random.Range(0f, 100f);
		if(tauntChance > tauntProbability)
		{
			// Wait for tauntDelay number of seconds.
			yield return new WaitForSeconds(tauntDelay);
			
			// If there is no clip currently playing.
			if(!audio.isPlaying)
			{
				// Choose a random, but different taunt.
				tauntIndex = TauntRandom();
				
				// Play the new taunt.
				audio.clip = taunts[tauntIndex];
				audio.Play();
			}
		}
	}
	
	
	int TauntRandom()
	{
		// Choose a random index of the taunts array.
		int i = Random.Range(0, taunts.Length);
		
		// If it's the same as the previous taunt...
		if(i == tauntIndex)
			// ... try another random taunt.
			return TauntRandom();
		else
			// Otherwise return this index.
			return i;
	}
	
	public void SetCanClimb(bool toggle) {
		canClimb = toggle;
		if (!toggle) {
			isClimb = false;
		}
	}

	public void SetLadder(GameObject ld) {
		ladder = ld;
	}

	public void SetOnRope(bool toggle) {
		isOnRope = toggle;
		if (!toggle) {
			canClimbRope = true;
		} else {
			rigidbody2D.velocity = new Vector2(0, 0);
		}
	}
	
}
