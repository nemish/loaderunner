﻿using UnityEngine;
using System.Collections;

public class WinTrigger : MonoBehaviour {

	private int winScreen;
	public GameObject ladder;
	public int stageCount;
	public Transform startingPosition;

	private static WinTrigger s_Instance = null;
	
	// This defines a static instance property that attempts to find the manager object in the scene and
	// returns it to the caller.
	public static WinTrigger instance {
		get {
			if (s_Instance == null) {
				// This is where the magic happens.
				//  FindObjectOfType(...) returns the first AManager object in the scene.
				s_Instance =  FindObjectOfType(typeof (WinTrigger)) as WinTrigger;
			}
			
			// If it is still null, create a new instance
			if (s_Instance == null) {
				GameObject obj = new GameObject("WinTrigger");
				s_Instance = obj.AddComponent(typeof (WinTrigger)) as WinTrigger;
				Debug.Log ("Could not locate an WinTrigger object. \n WinTrigger was Generated Automaticly.");
			}
			
			return s_Instance;
		}
	}
	
	// Ensure that the instance is destroyed when the game is stopped in the editor.
	void OnApplicationQuit() {
		s_Instance = null;
	}

	void Awake () {
		winScreen = 0; //TODO:Set scene id here
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col)
	{
		if (triggerGold.gold == CameraGoldPoints.goldAtLevel)
			Application.LoadLevel(winScreen);
	}

	public IEnumerator BuildLadder () {
		Debug.Log("Build");
		for (int i = 0; i < stageCount; i++) {
			yield return new WaitForSeconds(0.5f);
			Instantiate(ladder, new Vector2(0.0f, i*ladder.renderer.bounds.size.y + startingPosition.position.y), Quaternion.identity);
		}
	}
}
