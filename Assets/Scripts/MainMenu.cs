using UnityEngine;
using System.Collections;

public class MainMenu : MonoBehaviour {

    public Texture bgTexture;
//    private string instructionText = "Instuructions:\nSome...";
    private int buttonWidth = 200;
    private int buttonHeight = 50;
//   public static int HitsNeed = 25;
//    public static int AsteroidCount = 1;

    void OnGUI()
    {
        GUI.DrawTexture(new Rect(0, 0, Screen.width, Screen.height), bgTexture);
//        GUI.Label(new Rect(10, 10, 200, 200), instructionText);
        if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2,
                   Screen.height / 2 - buttonHeight / 2 - 60, buttonWidth, buttonHeight), "Easy"))
        {
//            HitsNeed = 25;
//            AsteroidCount = 1;
            Application.LoadLevel(0);
        }

        if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2,
                  Screen.height / 2 - buttonHeight / 2, buttonWidth, buttonHeight), "Normal"))
        {
 //           HitsNeed = 50;
//            AsteroidCount = 2;
            Application.LoadLevel(1);
        }

        if (GUI.Button(new Rect(Screen.width / 2 - buttonWidth / 2,
                  Screen.height / 2 - buttonHeight / 2 + 60, buttonWidth, buttonHeight), "Hard"))
        {
  //          HitsNeed = 100;
  //          AsteroidCount = 3;
            Application.LoadLevel(2);
        }
    }

    void Update()
    {
        if (Input.GetKeyDown("escape"))
            Application.Quit();
    }

}
