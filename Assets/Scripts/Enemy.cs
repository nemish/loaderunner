﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour, Destroyable
{
	public float moveSpeed = 2f;		// The speed the enemy moves at.
	public int HP = 1;					// How many times the enemy can be hit before it dies.
	public Sprite deadEnemy;			// A sprite of the enemy when it's dead.
	public Sprite damagedEnemy;			// An optional sprite of the enemy when it's damaged.
	public AudioClip[] deathClips;		// An array of audioclips that can play when the enemy dies.
	public GameObject hundredPointsUI;	// A prefab of 100 that appears when the enemy dies.
	public float deathSpinMin = -100f;			// A value to give the minimum amount of Torque when dying
	public float deathSpinMax = 100f;			// A value to give the maximum amount of Torque when dying
	public bool isTrapped = false;
	
	private Transform frontCheck;		// Reference to the position of the gameobject used for checking if something is in front.
	private bool dead = false;			// Whether or not the enemy is dead.
	private Score score;				// Reference to the Score script.
	private Transform player;

	private RaycastHit2D blockUnderEnemy;
	private Transform groundCheck;

	void Awake()
	{
		// Setting up the references.
		
		groundCheck = transform.Find("groundCheck");
		player = GameObject.FindGameObjectWithTag("Player").transform;
		frontCheck = transform.Find("frontCheck").transform;
//		score = GameObject.Find("Score").GetComponent<Score>();
	}
	void Update () {
		blockUnderEnemy = Physics2D.Linecast(transform.position, groundCheck.position, 1 << LayerMask.NameToLayer("Ground"));  
	}


	void FixedUpdate ()
	{
		if (!isTrapped) {
			MovementManagement();		
		}
	}

	public void DestroyByBrickWall() {
		Death ();
	}

	public void Reset() {
		dead = false;
		Collider2D[] cols = GetComponents<Collider2D>();
		foreach(Collider2D c in cols)
		{
			c.enabled = true;
			c.isTrigger = false;
		}

		GetComponent<Enemy> ().enabled = true;

		SpriteRenderer[] otherRenderers = GetComponentsInChildren<SpriteRenderer>();
		
		// Disable all of them sprite renderers.
		foreach(SpriteRenderer s in otherRenderers)
		{
			s.enabled = true;
		}
		rigidbody2D.fixedAngle = true;
		isTrapped = false;
		GetComponent<Animator> ().enabled = true;
		HP++; 
	}

	void MovementManagement () 
	{
		// Create an array of all the colliders in front of the enemy.
		Collider2D[] frontHits = Physics2D.OverlapPointAll(frontCheck.position, 1 << LayerMask.NameToLayer("Ground"));

		// Check each of the colliders.
		foreach(Collider2D c in frontHits)
		{
			// If any of the colliders is an Obstacle...
			if(c.tag == "Destroyable" || c.tag == "Rock")
			{
				Debug.Log (frontHits);
				// ... Flip the enemy and stop checking the other colliders.
				Flip ();
				break;
			}
		}
	
		if (blockUnderEnemy && blockUnderEnemy.transform.gameObject.tag == "Destroyable" && blockUnderEnemy.collider.isTrigger) {
			rigidbody2D.velocity = new Vector2 (0, rigidbody2D.velocity.y);
		} 
		else 
		{
			rigidbody2D.velocity = new Vector2(-(transform.localScale.x) * moveSpeed, rigidbody2D.velocity.y);	
		}

	}
	
	public void Death()
	{
		// Find all of the sprite renderers on this object and it's children.
		SpriteRenderer[] otherRenderers = GetComponentsInChildren<SpriteRenderer>();

		// Disable all of them sprite renderers.
		foreach(SpriteRenderer s in otherRenderers)
		{
			s.enabled = false;
		}

		// Re-enable the main sprite renderer and set it's sprite to the deadEnemy sprite.
//		ren.enabled = true;
//		ren.sprite = deadEnemy;

		// Increase the score by 100 points
//		score.score += 100;

		// Set dead to true.
		dead = true;

		// Allow the enemy to rotate and spin it by adding a torque.
		rigidbody2D.fixedAngle = false;
		rigidbody2D.AddTorque(Random.Range(deathSpinMin,deathSpinMax));

		// Find all of the colliders on the gameobject and set them all to be triggers.
		Collider2D[] cols = GetComponents<Collider2D>();
		foreach(Collider2D c in cols)
		{
			c.isTrigger = true;
		}

		// Play a random audioclip from the deathClips array.
		int i = Random.Range(0, deathClips.Length);
		AudioSource.PlayClipAtPoint(deathClips[i], transform.position);

		// Create a vector that is just above the enemy.
		Vector3 scorePos;
		scorePos = transform.position;
		scorePos.y += 1.5f;

		// Instantiate the 100 points prefab at this point.
		Instantiate(hundredPointsUI, scorePos, Quaternion.identity);
	}


	public void Flip()
	{
		// Multiply the x component of localScale by -1.
		Vector3 enemyScale = transform.localScale;
		enemyScale.x *= -1;
		transform.localScale = enemyScale;
	}
}
